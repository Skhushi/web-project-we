# Binge Watch - Movie Recommendation Website

This website provides all the details of the requested movie such as overview, genre, release date, rating, runtime, top cast, reviews, recommended movies, etc. It is created using Collaborative Filtering (Website) and Content based Filtering (Jupyter Notebook).

<img width="1432" alt="Screen Shot 2022-05-29 at 12 19 51 PM" src="Images/Pic1.jpg">

The details of the movies(title, genre, runtime, rating, poster, etc) are fetched using an API by TMDB, https://www.themoviedb.org/documentation/api, and using the IMDB id of the movie in the API.



 
  
<a id="resources"></a>
## 📚 Resources
  ### Data Set Used 
1. [IMDB 5000 Movie Dataset](https://www.kaggle.com/carolzhangdc/imdb-5000-movie-dataset)
2. [The Movies Dataset](https://www.kaggle.com/rounakbanik/the-movies-dataset)
3. [List of movies in 2018](https://en.wikipedia.org/wiki/List_of_American_films_of_2018)
4. [List of movies in 2019](https://en.wikipedia.org/wiki/List_of_American_films_of_2019)
5. [List of movies in 2020](https://en.wikipedia.org/wiki/List_of_American_films_of_2020)

- [JS Tutorials](https://www.w3schools.com/js/)
- [HTML](https://www.html.am)
- [Python](https://docs.python.org/3/)
